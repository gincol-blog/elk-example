package es.gincol.ms.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.gincol.ms.utils.Constants;

@Component
public class RequestFilter implements Filter {

	@Value("${spring.application.name}")
	private String applicationName;
	
	@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            MDC.put(Constants.SERVICE_NAME_HEADER_KEY, applicationName);
            chain.doFilter(request, response);
        } finally {
           MDC.clear();
        }
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
}
