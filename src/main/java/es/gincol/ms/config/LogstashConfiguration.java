package es.gincol.ms.config;

import javax.annotation.PostConstruct;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import net.logstash.logback.appender.LogstashTcpSocketAppender;
import net.logstash.logback.encoder.LogstashEncoder;

@Component
@ConditionalOnProperty(name = "logstash.enabled", havingValue = "true", matchIfMissing = false)
public class LogstashConfiguration {

	private LogstashTcpSocketAppender logstashTcpSocketAppender;

	@Value("${logstash.server.host}")
	String logstashServerHost;
	
	@Value("${logstash.server.port}")
	String logstashServerPort;
	
	@PostConstruct
    public void init() {
        Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        LogstashEncoder encoder = new LogstashEncoder();
        
        logstashTcpSocketAppender = new LogstashTcpSocketAppender();
        logstashTcpSocketAppender.setName("logstash");
        logstashTcpSocketAppender.setContext(context);
        logstashTcpSocketAppender.addDestination(logstashServerHost+":"+logstashServerPort);
        
        logstashTcpSocketAppender.setEncoder(encoder);
        logstashTcpSocketAppender.start();

        rootLogger.addAppender(logstashTcpSocketAppender);
        rootLogger.setLevel(Level.INFO);
        
	}
}
