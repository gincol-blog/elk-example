package es.gincol.ms.utils;

public final class Constants {

	public static final String SPRING_PROFILE_DESARROLLO = "des";
	public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";
    
    public static final String SERVICE_NAME_HEADER_KEY = "servicename";

}
