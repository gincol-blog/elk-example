package es.gincol.ms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	private static final Logger log = LoggerFactory.getLogger(MainController.class);
	
	@GetMapping(path = "/whoami/{username}")
	public String whoami(@PathVariable("username") String username) {
		log.info("acceso con username: " + username);
		return String.format("Hola! %s", username);
	}
	
}
